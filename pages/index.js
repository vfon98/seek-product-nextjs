import React from 'react';
import { useEffect } from 'react';
import { useRouter } from 'next/router';

const App = () => {
  const router = useRouter();
  useEffect(() => {
    router.push('/products/[category]', '/products/All', { shallow: true });
  });

  return <h1>Welcome to my App</h1>;
};

export default App;
