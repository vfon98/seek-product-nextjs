import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { registerAccount } from '../../actions/auth.actions';
import useInput from '../../hooks/useInput';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Row, Col, Input, Button, Card, CardHeader, CardBody } from 'reactstrap';

const SignupForm = () => {
  const [firstName, bindFirstName] = useInput('Phong');
  const [lastName, bindLastName] = useInput('To');
  const [email, bindEmail] = useInput('quocduby@gmail.com');
  const [password, bindPassword] = useInput('password');
  const [confirmPassword, bindConfirmPassword] = useInput('password');
  const [city, bindCity] = useInput('Can Tho');

  const [passwordError, setPasswordError] = useState('');

  const router = useRouter();
  const dispatch = useDispatch();
  const auth = useSelector(state => state.authReducer);
  const error = auth.registerError;

  useEffect(() => {
    if (auth.registerSuccess) {
      router.push('/login');
    }
  }, [auth.registerSuccess]);

  const handleSubmit = e => {
    e.preventDefault();
    if (password !== confirmPassword) {
      setPasswordError('Password does not match');
      return;
    }

    // Object key based on API input data
    const account = {
      first_name: firstName,
      last_name: lastName,
      email,
      password,
      confirm_password: confirmPassword,
      city,
    };
    dispatch(registerAccount(account));
  };

  const resetError = () => setPasswordError('');

  return (
    <Row>
      <Col md={{ size: 6, offset: 3 }} sm={{ size: 8, offset: 2 }}>
        <Card className='text-center'>
          <CardHeader className='bg-success text-white'>
            <strong>First time with us? Sign up now!</strong>
          </CardHeader>
          <CardBody>
            <form role='form' onSubmit={handleSubmit}>
              <fieldset>
                <Row>
                  <div className='mx-auto'>
                    <img
                      className='mb-3 rounded-circle'
                      src='/static/images/user-signup.png'
                      alt='missing'
                    />
                  </div>
                </Row>
                <Row className='text-left'>
                  <Col md={{ size: 10, offset: 1 }} sm='12' className='px-0'>
                    <div className='form-group'>
                      <label>First name</label>
                      <Input
                        placeholder='Ex: John'
                        type='text'
                        {...bindFirstName}
                        autoFocus
                        required
                      />
                    </div>
                    <div className='form-group'>
                      <label>Last name</label>
                      <Input
                        placeholder='Ex: Doe'
                        type='text'
                        {...bindLastName}
                        required
                      />
                    </div>
                    <div className='form-group'>
                      <label>Email</label>
                      <Input
                        placeholder='Ex: example@mail.com'
                        type='email'
                        {...bindEmail}
                        required
                      />
                      <div className='error-input'>
                        {error && error.email && error.email[0]}
                      </div>
                    </div>
                    <div className='form-group'>
                      <label>Password</label>
                      <Input
                        placeholder='At least 4 characters'
                        type='password'
                        {...bindPassword}
                        required
                      />
                      {/* <div className='error-input'>{passwordError}</div> */}
                    </div>
                    <div className='form-group'>
                      <label>Confirm password</label>
                      <Input
                        onBlur={resetError}
                        onInput={resetError}
                        placeholder='At least 4 characters'
                        type='password'
                        {...bindConfirmPassword}
                        required
                      />
                      <div className='error-input'>
                        {passwordError}
                      </div>
                    </div>
                    <div className='form-group'>
                      <label>City (For shipping)</label>
                      <Input
                        placeholder='Ex: Can Tho'
                        type='text'
                        {...bindCity}
                      />
                    </div>
                    <div className='form-group'>
                      <Button type='submit' color='primary' size='lg' block>
                        <i className='fa fa-user-plus mr-2'></i>
                        Sign up
                      </Button>
                    </div>
                    <hr className='mb-1' />
                    <div className='text-center'>Or</div>
                    <Link href='/login'>
                      <Button color='success' size='lg' block>
                        <i className='fa fa-sign-in mr-2'></i>
                        Login
                      </Button>
                    </Link>
                  </Col>
                </Row>
              </fieldset>
            </form>
          </CardBody>
        </Card>
      </Col>
    </Row>
  );
};

export default SignupForm;
