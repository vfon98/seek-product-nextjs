import React from 'react';

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

function Error({ statusCode }) {
  return (
    <div className='jumbotron text-center' id='not-found-page'>
      <i className='fa fa-frown-o text-danger'></i>
      <h1>{statusCode} - Page not found</h1>
      <code>Please check your URL and try again!</code>
    </div>
  );
}

export default Error;
