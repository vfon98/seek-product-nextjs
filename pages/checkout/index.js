import React, { useState, useEffect } from 'react';
import Checkout from '../../components/Checkout/Checkout';
import { StripeProvider, Elements } from 'react-stripe-elements';
import withAuth from '../../HOCs/withAuth';

function CheckoutPage() {
  // const PERSONAL_STRIPE_KEY = 'pk_test_QOOyeVrCYofsYsT36rGSO9Ij00IaJ3SQYt';
  const COMPANY_STRIPE_KEY = 'pk_test_7X4at47jVmUqka7N8HhdO35N';
  const [stripe, setStripe] = useState(null);

  useEffect(() => {
    setStripe(window.Stripe(COMPANY_STRIPE_KEY));
  }, []);

  return (
    <>
      <StripeProvider stripe={stripe}>
        <Elements>
          <Checkout />
        </Elements>
      </StripeProvider>
    </>
  );
}

export default withAuth(CheckoutPage);
