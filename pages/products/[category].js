import React from 'react';
import { connect } from 'react-redux';
import { searchByCategory } from '../../actions/products.actions';
import { fetchCategories } from '../../actions/categories.actions';
import Categories from '../../components/Products/Categories';
import Pagination from '../../components/Products/Pagination';
import ProductsList from '../../components/Products/ProductsList';
import LoadingButton from '../../components/LoadingButton';
import { withRouter } from 'next/router';
import { deformatURL } from '../../utils/url.formater';

class Products extends React.Component {
  static async getInitialProps({ store, isServer }) {
    // process.browser &&
    store.dispatch(searchByCategory('All'));
    store.dispatch(fetchCategories());
    return { isServer };
  }

  componentDidMount() {
    const { query } = this.props.router;
    this.props.fetchCategories();
    this.props.searchByCategory(deformatURL(query.category));
  }

  componentDidUpdate(prevProps) {
    const { query } = this.props.router;
    if (prevProps.router.query.category !== query.category) {
      this.props.searchByCategory(deformatURL(query.category));
    }
  }

  render() {
    const { products, isLoading } = this.props;

    return (
      <>
        <Categories />
        <hr />
        {isLoading ? (
          <LoadingButton />
        ) : (
          <>
            <ProductsList products={products} />
            <Pagination />
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.authReducer,
    products: state.productsReducer.products,
    isLoading: state.productsReducer.isLoading,
  };
};

const mapDispatchToProps = dispatch => ({
  searchByCategory: category => dispatch(searchByCategory(category)),
  fetchCategories: () => dispatch(fetchCategories()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Products));
