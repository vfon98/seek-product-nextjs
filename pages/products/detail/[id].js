import './detail.style.scss';
import React, { Component } from 'react';
import dynamic from 'next/dynamic'
import { connect } from 'react-redux';
import { detailProduct } from '../../../actions/products.actions';
import ProductImages from '../../../components/Details/ProductImages';
import ProductInfo from '../../../components/Details/ProductInfo';
import CompanyInfo from '../../../components/Details/CompanyInfo';
import LoadingButton from '../../../components/LoadingButton';
import { withRouter } from 'next/router';
import { Row, Button } from 'reactstrap';
const ProductDescription = dynamic(() => import('../../../components/Details/ProductDescription'), {
  loading: () => <LoadingButton />
});
// import OtherProducts from '../../../components/Details/OtherProducts';
const OtherProducts = dynamic(() => import('../../../components/Details/OtherProducts'), {
  loading: () => <LoadingButton />
});

export class ProductDetail extends Component {
  static async getInitialProps({ store, isServer, query }) {
    store.dispatch(detailProduct(query.id));
    return { isServer }
  }

  componentDidMount() {
    const { query } = this.props.router;
    this.props.detailProduct(query.id);
  }

  componentDidUpdate(prevProps) {
    const { query } = this.props.router;
    if (prevProps.router.query.id !== query.id) {
      this.props.detailProduct(query.id);
    }
  }

  render() {
    const { details, isLoading } = this.props;

    return isLoading ? (
      <LoadingButton />
    ) : (
      <React.Fragment>
        <Button
          color='link'
          onClick={() =>
            this.props.router.push('/products/[category]', '/products/All')
          }
        >
          <i className='fa fa-chevron-left fa-lg mr-2'></i>Back
        </Button>
        <Row className='shadow'>
          <ProductImages image={details.product.image} feeds={details.feeds} />
          <ProductInfo product={details.product} />
          <CompanyInfo company={details.company} />
        </Row>
        <Row>
          <ProductDescription description={details.product.full_description} />
        </Row>
        <hr />
        <h2>Other products</h2>
        <Row>
          <OtherProducts products={details.other_products} />
        </Row>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  details: state.productsReducer.details,
  isLoading: state.productsReducer.isLoading
});

const mapDispatchToProps = dispatch => ({
  detailProduct: id => dispatch(detailProduct(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ProductDetail));
