// eslint-disable-next-line no-unused-vars
import 'bootstrap/dist/css/bootstrap.min.css';
import '../index.scss';
import App from 'next/app';
import React from 'react';
import Layout from '../layouts/Layout';
import { Provider } from 'react-redux';
import createStore from '../store/configureStore';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps, store } = this.props;
    return (
      <Provider store={store}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Provider>
    );
  }
}

export default withRedux(createStore)(withReduxSaga(MyApp));
// export default withReduxSaga(MyApp);
