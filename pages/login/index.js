import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { login } from '../../actions/auth.actions';
import useInput from '../../hooks/useInput';
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Input,
  Button
} from 'reactstrap';
import Link from 'next/link';
import { useRouter } from 'next/router';

const LoginForm = () => {
  const [username, bindUsername] = useInput('test1@gmail.com');
  const [password, bindPassword] = useInput('password');
  const router = useRouter();

  const dispatch = useDispatch();
  const auth = useSelector(state => state.authReducer);

  useEffect(() => {
    auth.isLogin && router.back();
  }, [auth.isLogin]);

  const handleSubmit = e => {
    e.preventDefault();
    dispatch(login(username, password));
  };

  return (
    <Row className='justify-content-center'>
      <Col sm='5'>
        <Card className='text-center'>
          <CardHeader className='bg-success text-white'>
            <strong>You need to login first!</strong>
          </CardHeader>
          <CardBody>
            <form role='form' onSubmit={handleSubmit}>
              <fieldset>
                <Row>
                  <div className='mx-auto'>
                    <img
                      className='mb-3 rounded-circle'
                      src='/static/images/user-login.png'
                      alt='Missing'
                    />
                  </div>
                </Row>
                <Row>
                  <Col sm='12' md={{ size: 10, offset: 1 }} className='px-0'>
                    <div className='form-group'>
                      <div className='input-group'>
                        <span className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fa fa-user'></i>
                          </span>
                        </span>
                        <Input
                          placeholder='Username'
                          type='text'
                          {...bindUsername}
                          autoFocus
                        />
                      </div>
                    </div>
                    <div className='form-group'>
                      <div className='input-group'>
                        <span className='input-group-prepend'>
                          <span className='input-group-text'>
                            <i className='fa fa-lock'></i>
                          </span>
                        </span>
                        <Input
                          placeholder='Password'
                          type='password'
                          onInput={() =>
                            auth.loginError && (auth.loginError.email = '')
                          }
                          {...bindPassword}
                        />
                      </div>
                      {/* LOGIN ERROR */}
                      <div className='error-input'>
                        {auth.loginError && auth.loginError.email}
                      </div>
                    </div>
                    <div className='form-group'>
                      <Button
                        type='submit'
                        color='primary'
                        size='lg'
                        block
                        disabled={auth.isAuthPending}
                      >
                        {auth.isAuthPending ? <i className='fa fa-spin fa-cog fa-lg mr-2'></i> : ''}
                        Sign in
                      </Button>
                    </div>
                  </Col>
                </Row>
              </fieldset>
            </form>
          </CardBody>
          <CardFooter>
            Don&apos;t have an account! <br />
            <Link href='/signup'>
              <a>Sign Up Here</a>
            </Link>
          </CardFooter>
        </Card>
      </Col>
    </Row>
  );
};

export default LoginForm;
