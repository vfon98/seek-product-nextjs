import React from 'react';
import { useSelector } from 'react-redux';
import Link from 'next/link';
import { Jumbotron, Row, Col, Button } from 'reactstrap';
import withAuth from '../../HOCs/withAuth';

const UserInfo = () => {
  const cart = useSelector(state => state.cartReducer);
  const userInfo = useSelector(state => state.userReducer);

  return (
    <>
      <Jumbotron>
        <Row className='align-items-center'>
          <Col md='6' className='pb-3 text-center'>
            <img
              src='/static/images/user.png'
              className='img-fluid w-75'
              alt='User Information'
            />
          </Col>
          <Col md='6'>
            <h2 className='text-success font-weight-bold'>User information</h2>
            <hr />
            <p>
              <b>First name: </b>
              {userInfo.firstName}
            </p>
            <p>
              <b>Last name: </b>
              {userInfo.lastName}
            </p>
            <p>
              <b>Email: </b>
              {userInfo.email}
            </p>
            <p>
              <b>City: </b>
              {userInfo.city || 'Unkown'}
            </p>
            <p>
              <b>Total items in cart: </b>
              {cart.cart_detail.length}
            </p>
            <p>
              <b>Total cart price: </b>${cart.total.toLocaleString('en-EN')}
            </p>
            <Link href='/update-user'>
              <Button color='danger' size='lg'>
                <>
                  <i className='fa fa-wrench fa-lg mr-2'></i>
                  Change information
                </>
              </Button>
            </Link>
          </Col>
        </Row>
      </Jumbotron>
    </>
  );
};

export default React.memo(withAuth(UserInfo));
