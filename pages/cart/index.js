import './cart.style.scss';
import React from 'react';
import UserCart from '../../components/Cart/UserCart';

function Cart() {
  return <UserCart />;
}

export default Cart;
