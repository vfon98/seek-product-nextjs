import axios from '../utils/axios.base';
import { user } from '../constants/endpoints';

export const getUserProfile = token => {
  return axios.get(user.USER_PROFILE, {
    headers: { Authorization: `JWT ${token}` }
  });
};

export const updateProfile = (token, newProfile) => {
  const body = { ...newProfile };
  return axios.put(user.UPDATE_PROFILE, body, {
    headers: { Authorization: `JWT ${token}` }
  });
};
