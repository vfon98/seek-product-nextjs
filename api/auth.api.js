import { auth } from '../constants/endpoints';
import axios from '../utils/axios.base';

export const registerAccount = accountInfo => {
  return axios.post(auth.REGISTER, accountInfo);
};

export const login = (email, password) => {
  const body = { email, password };
  return axios.post(auth.LOGIN, body);
};

export const logout = token => {
  return axios.get(auth.LOGOUT, { headers: { Authorization: `JWT ${token}` } });
};

export const updatePassword = (token, body) => {
  return axios.put(auth.UPDATE_PASSWORD, body, {
    headers: { Authorization: `JWT ${token}` }
  });
};
