import axios from '../utils/axios.base';
import { category } from '../constants/endpoints';

export const fetchCategories = () => {
  return axios.get(category.CATEGORY_LIST);
};
