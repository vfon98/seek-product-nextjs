import axios from 'axios';
import { firebase } from '../constants/endpoints';

export const sendFCM = (fcm_token, data) => {
  const body = {
    to: fcm_token,
    data: {
      title: data.title,
      body: data.body,
      icon: data.icon,
    },
  };
  return axios.post(firebase.FCM_SENDER, body, {
    headers: {
      Authorization: `key=AAAAHP7B-7M:APA91bGFIkQDSuEA-XZE-rh6jEh_lRRnLz0rg3dH8yjB40MamjZ2i5lmZCNlTYykbrN5f0LESwlxiQL9qPnzeg5ETPxr5LAroqNV6W9glfxYjeVceuWrmoyI1QnaaXsRC6Wv2Kz0iDNM`,
    },
  });
};
