import * as types from '../constants/actionTypes';

export const nextPage = () => {
  return { type: types.NEXT_PAGE };
};

export const previousPage = () => {
  return { type: types.PREVIOUS_PAGE };
};

export const gotoPage = page => {
  return { type: types.GO_TO_PAGE, payload: { page } };
};

export const resetPage = () => {
  return { type: types.GO_TO_PAGE_OK, payload: { page_number: 1 } };
};
