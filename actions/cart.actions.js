import * as types from '../constants/actionTypes';

export const fetchCart = token => {
  return {
    type: types.FETCH_CART,
    payload: { token },
  };
};

export const addToCart = (token, productID, amount = 1) => {
  return {
    type: types.ADD_TO_CART,
    payload: { token, productID, amount },
  };
};

export const addToGuestCart = (product, amount = 1) => {
  return {
    type: types.ADD_TO_GUEST_CART,
    payload: { product, amount },
  };
};

export const udpateCartItem = (token, productID, amount) => {
  return {
    type: types.UPDATE_CART_ITEM,
    payload: { token, productID, amount },
  };
};

export const deleteCartItem = (token, productID) => {
  return {
    type: types.DELETE_CART_ITEM,
    payload: { token, productID },
  };
};

export const clearCart = token => {
  return {
    type: types.CLEAR_CART,
    payload: { token },
  };
};

export const checkoutCart = (token, stripe_token) => {
  return {
    type: types.CHECK_OUT_CART,
    payload: { token, stripe_token },
  };
};
