import * as types from '../constants/actionTypes';

export const fetchProductsAction = products => ({
  type: types.FETCH_PRODUCTS,
  payload: products,
});

export const searchByCategory = category => {
  return { type: types.SEARCH_PRODUCTS, payload: { category } };
};

export const detailProduct = id => {
  return { type: types.DETAIL_PRODUCT, payload: { id } };
};
