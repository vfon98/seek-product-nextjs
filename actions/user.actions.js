import * as types from '../constants/actionTypes';

export const authUser = () => ({ type: types.AUTH_USER });

export const getUserProfile = token => {
  return {
    type: types.GET_USER_PROFILE,
    payload: { token }
  };
  // return dispatch => {
  //   UserAPI.getUserProfile(token)
  //     .then(res => {
  //       dispatch({
  //         type: types.GET_USER_PROFILE,
  //         payload: { profile: res.data }
  //       });
  //     })
  //     .catch(err => console.log({ err }));
  // };
};

export const updateProfile = (token, oldProfile) => {
  const newProfile = {
    first_name: oldProfile.firstName,
    last_name: oldProfile.lastName,
    city: oldProfile.city
  };
  return { type: types.UPDATE_PROFILE, payload: { token, newProfile } };
  // return async dispatch => {
  //   await UserAPI.updateProfile(token, newProfile)
  //     .then(res => {
  //       dispatch({
  //         type: types.UPDATE_PROFILE
  //       });
  //       dispatch(getUserProfile(token));
  //       Promise.resolve(res);
  //     })
  //     .catch(err => console.log({ err }));
  // };
};
