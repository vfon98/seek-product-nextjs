import * as types from '../constants/actionTypes';

export const fetchCategories = () => ({ type: types.FETCH_CATEGORIES });

export const changeSelectedCategory = categoryName => {
  return {
    type: types.CHANGE_SELECTED_CATEGORY,
    payload: { categoryName },
  };
};
