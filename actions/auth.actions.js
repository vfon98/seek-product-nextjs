import * as types from '../constants/actionTypes';

export const authUser = () => {
  return {
    type: types.AUTH_USER
  };
};

export const registerAccount = accountInfo => {
  return {
    type: types.REGISTER_ACCOUNT,
    payload: { accountInfo }
  };
};

export const login = (email, password) => {
  return {
    type: types.LOGIN_ACCOUNT,
    payload: { email, password }
  };
};

export const logout = token => {
  return { type: types.LOGOUT, payload: { token } };
};

export const updatePassword = (token, input) => {
  const body = {
    old_password: input.oldPassword,
    new_password: input.newPassword,
    confirm_password: input.confirmPassword
  };
  return {
    type: types.UPDATE_PASSWORD,
    payload: { token, body }
  };
};
