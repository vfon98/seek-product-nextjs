const express = require('express');
const next = require('next');
const dev = process.env.NODE_DEV !== 'production';
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();
const port = process.env.PORT || 3000;
const proxy = require('http-proxy-middleware');

nextApp.prepare().then(() => {
  const app = express();
  // app.all('/api', function(req, res, next) {
  //   console.log("get API")
  //   res.header('Access-Control-Allow-Origin', '*'); // update to match the domain you will make the request from
  //   next();
  // });

  // app.use('/api', proxy({
  //   target: 'http://localhost:4200',
  //   changeOrigin: true
  // }))

  app.get('*', (req, res) => {
    return handle(req, res);
  });

  app.listen(port, () => {
    console.log(
      'LISTENING ON PORT %s at %s',
      port,
      new Date().toLocaleString('en-EN')
    );
  });
});
