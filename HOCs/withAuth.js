import React, { useEffect } from 'react';
import { useSelector } from 'react-redux'
import { useRouter } from 'next/router'

function withAuth(InnerComponent) {
  const Authentication = () => {
    const isLogin = useSelector(state => state.authReducer.isLogin)
    const router = useRouter();

    useEffect(() => {
      !isLogin && router.push('/login')
    }, [])

    return <InnerComponent />
  }
  return Authentication;
}

export default withAuth;
