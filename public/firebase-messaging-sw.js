/* eslint-disable no-undef */
importScripts('https://www.gstatic.com/firebasejs/7.6.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.2/firebase-analytics.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.2/firebase-messaging.js');

firebase.initializeApp({
  apiKey: 'AIzaSyDHdEhhWGoFJmuNMbNQ4KcjFhz8dp01kEI',
  authDomain: 'seek-product-nextjs.firebaseapp.com',
  databaseURL: 'https://seek-product-nextjs.firebaseio.com',
  projectId: 'seek-product-nextjs',
  storageBucket: 'seek-product-nextjs.appspot.com',
  messagingSenderId: '124533210035',
  appId: '1:124533210035:web:a094f0ba4388fbef1ea9c6',
  measurementId: 'G-NP0LJ444E7',
});

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('Handling background message ', payload);

  return self.registration.showNotification(payload.data.title, {
    body: payload.data.body,
    icon: payload.data.icon,
    tag: payload.data.tag,
    data: payload.data.link,
  });
});

self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  // event.waitUntil(self.clients.openWindow(event.notification.data));
  event.waitUntil(self.clients.openWindow(event.target.origin));
});
