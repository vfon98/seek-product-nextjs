/* eslint-disable no-case-declarations */
import * as types from '../constants/actionTypes';

const inintialState = {
  isChecking: null,
  isPending: false,
  hasError: false,
  id: null,
  total: 0,
  status: null,
  user: null,
  paid_at: null,
  cart_detail: [],
};

const cartReducer = (state = inintialState, action) => {
  switch (action.type) {
    case types.FETCH_CART_OK:
      return {
        ...state,
        ...action.payload.cart,
        // Append guest card with cart user cart from API
        // cart_detail: [...state.cart_detail, ...action.payload.cart.cart_detail],
      };

    case types.FETCH_GUEST_CART_OK:
      return {
        ...state,
        cart_detail: action.payload.guest_cart,
      };

    case types.ADD_TO_CART_OK:
      return {
        ...state,
        ...action.payload.cart,
      };

    case types.ADD_TO_GUEST_CART_OK:
      return {
        ...state,
        cart_detail: [...state.cart_detail, action.payload.cart_item],
      };

    case types.UPDATE_CART_ITEM:
      return {
        ...state,
        isPending: true,
      };

    case types.UPDATE_CART_ITEM_OK:
      return {
        ...state,
        ...action.payload.cart,
        isPending: false,
      };

    case types.UPDATE_GUEST_CART_OK:
      let { productID, amount } = action.payload;
      return {
        ...state,
        isPending: false,
        cart_detail: state.cart_detail.map(item =>
          item.product.id === productID
            ? { ...item, amount: amount, total: item.product.price * amount }
            : item
        ),
      };

    case types.DELETE_CART_ITEM:
      return {
        ...state,
        isPending: true,
      };

    case types.DELETE_CART_ITEM_OK:
      return {
        ...state,
        ...action.payload.cart,
        isPending: false,
      };

    case types.DELETE_GUEST_CART_OK:
      console.log('IM HERE');
      return {
        ...state,
        isPending: false,
        cart_detail: state.cart_detail.filter(
          item => item.product.id !== action.payload.productID
        ),
      };

    case types.CLEAR_CART_OK:
      return {
        ...state,
        ...action.payload.cart,
      };

    case types.CLEAR_GUEST_CART_OK:
      return {
        ...state,
        cart_detail: [],
      };

    case types.CHECK_OUT_CART:
      return {
        ...state,
        isChecking: true,
      };

    case types.CHECK_OUT_OK:
      return {
        ...state,
        isChecking: false,
        cart_detail: [],
        total: 0,
        paid_at: new Date().toISOString(),
      };

    case types.CHECK_OUT_FAILED:
      return {
        ...state,
        isChecking: false,
        hasError: true,
      };

    case types.LOGOUT:
      return inintialState;

    default:
      return state;
  }
};

export default cartReducer;
