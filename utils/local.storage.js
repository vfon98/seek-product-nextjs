export const removeTokenFromLocalStorage = () => {
  let token = localStorage.getItem('user-token');
  token && localStorage.removeItem('user-token');
  console.log('REMOVED TOKEN');
};

export const saveTokenIntoLocalStorage = token => {
  localStorage.setItem('user-token', token);
};

export const getTokenFromLocalStorage = () => {
  return localStorage.getItem('user-token');
};

export const getCartFromLocalStorage = () => {
  console.log('GET CART FROM LS');
  // Return [] instead of null
  return JSON.parse(localStorage.getItem('user-cart')) || [];
};

export const updateCartInLocalStorage = cart => {
  localStorage.setItem('user-cart', JSON.stringify(cart));
};

export const getGuestCartFromLocalStorage = () => {
  return JSON.parse(localStorage.getItem('guest-cart')) || [];
};

export const updateGuestCartFromLocalStorage = cart => {
  localStorage.setItem('guest-cart', JSON.stringify(cart));
};

export const removeGuestCartFromLocalStorage = () => {
  localStorage.removeItem('guest-cart');
}

export const getFCMToken = () => {
  return localStorage.getItem('fcm_token');
}
