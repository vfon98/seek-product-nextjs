import axios from 'axios';

const hostname = typeof window !== 'undefined' ? window.location.hostname : '';
console.log(hostname)
const baseURL = 'http://api.seekproduct.com';
const instance = axios.create({
  // Use proxy server to avoid CORS
  baseURL:
    hostname === 'localhost'
      ? baseURL
      : `https://cors-anywhere.herokuapp.com/${baseURL}`,
  headers: { 'X-Requested-With': 'XMLHttpRequest' },
});

export default instance;
