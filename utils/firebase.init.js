import * as firebase from 'firebase/app';
import 'firebase/messaging';
import 'firebase/analytics';

const firebaseConfig = {
  apiKey: 'AIzaSyDHdEhhWGoFJmuNMbNQ4KcjFhz8dp01kEI',
  authDomain: 'seek-product-nextjs.firebaseapp.com',
  databaseURL: 'https://seek-product-nextjs.firebaseio.com',
  projectId: 'seek-product-nextjs',
  storageBucket: 'seek-product-nextjs.appspot.com',
  messagingSenderId: '124533210035',
  appId: '1:124533210035:web:a094f0ba4388fbef1ea9c6',
  measurementId: 'G-NP0LJ444E7',
};

export const getFCMToken = () => {
  return localStorage.getItem('fcm_token');
};

export const initFCM = async () => {
  // Check if firebase has been loaded
  !firebase.apps.length && firebase.initializeApp(firebaseConfig);
  console.log('APP initialized !');
  try {
    firebase.analytics();
    const messaging = firebase.messaging();
    messaging.onMessage(payload => {
      console.log('MESSAGE RECEIVED:', payload);
    });
    
    if (getFCMToken() !== null) return false;
    // Jump to catch if Access denied
    await messaging.requestPermission();
    console.log("Access granted !")
    // usePublicVapidKey has to be called once before getToken
    messaging.usePublicVapidKey(
      'BMk-NokF8Qd5iXnEn2-4VdIVMc_F182oK7rQtxht8_SIebci16UapymUryPebMiTcNGhsazUNk_2cPDRIzINh4k'
    );
    const token = await messaging.getToken();
    localStorage.setItem('fcm_token', token);
    console.log('FCM token:', token);
  } catch (err) {
    console.log(err);
  }
};
