import React from 'react';
import dynamic from 'next/dynamic';
import { Row } from 'reactstrap';
// import OneProduct from './OneProduct';
const OneProduct = dynamic(() => import('./OneProduct'));
import EmptyButton from '../EmptyButton';

function ProductsList(props) {
  return props.products.length ? (
    <Row>
      {props.products.map(product => {
        return <OneProduct {...product} key={product.id} />;
      })}
    </Row>
  ) : (
    <EmptyButton />
  );
}

export default ProductsList;
