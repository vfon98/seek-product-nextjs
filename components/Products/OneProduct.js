import React, { useState } from 'react';
import { Col, Card, CardBody, Button } from 'reactstrap';
import { useRouter } from 'next/router';
import StarRating from '../StarRating';
import { useDispatch, useSelector } from 'react-redux';
import { addToCart, addToGuestCart } from '../../actions/cart.actions';

const OneProduct = props => {
  const [hasError, setHasError] = useState(false);
  const router = useRouter();
  const dispatch = useDispatch();
  const auth = useSelector(state => state.authReducer);

  const isAdded = () => false;

  const handleAddToCart = e => {
    e.stopPropagation();
    console.log('PRODUCT ID', props.id);

    // Add to temporary cart when user is not login
    if (auth.isLogin) {
      dispatch(addToCart(auth.token, props.id));
    } else {
      dispatch(addToGuestCart(props));
    }
  };

  const handleDetail = () => {
    router.push('/products/detail/[id]', `/products/detail/${props.id}`);
  };

  return (
    <Col md='3' sm='4' xs='6' className='mb-4'>
      <Card className='card-product shadow' onClick={handleDetail}>
        <img
          title={props.name}
          onError={() => setHasError(true)}
          className='card-img-top img-fluid border-bottom'
          src={
            !hasError && props.image
              ? props.image
              : 'https://via.placeholder.com/250?text=no+image'
          }
          alt='Missing image'
        />
        <CardBody className="pt-2">
          <h5
            className='card-text font-weight-bold'
            id='product-name'
            title={props.id}
          >
            {props.name}
          </h5>
          <p className='card-text font-weight-bolder text-muted mb-1'>
            ${props.price.toLocaleString('en-EN')}
          </p>
          <p className='card-text mb-1'>
            {props.category.map(category => category.name).join(', ')}
          </p>
          <p title={props.rating + ' stars'}>
            <StarRating rating={props.rating} />
          </p>
          <Button color='success' onClick={handleAddToCart}>
            <i
              className={'fa mr-2 ' + (isAdded() ? 'fa-check' : 'fa-cart-plus')}
            ></i>
            Add to cart
          </Button>
        </CardBody>
      </Card>
    </Col>
  );
};

export default React.memo(OneProduct);
