import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { formatURL, deformatURL } from '../../utils/url.formater';
import './product.style.scss';
import { changeSelectedCategory } from '../../actions/categories.actions';
import { NavLink, NavItem, Nav } from 'reactstrap'

function Categories() {
  const dispatch = useDispatch();
  const router = useRouter();
  const categories = useSelector(state => state.categoriesReducer);

  const isValidCategory = () => {
    const { query } = router;
    // categories inside categoriesReducer. Just same name
    return categories.categories.find(
      category => category.name === deformatURL(query.category)
    );
  };

  useEffect(() => {
    if (!categories.isLoading) {
      dispatch(changeSelectedCategory(router.query.category));
      !isValidCategory() && router.push('/404');
    }
  }, [categories.isLoading]);

  const handleChangeCategory = categoryName => {
    dispatch(changeSelectedCategory(categoryName));
  };

  const categoriesList = categories.categories.map(category => {
    const { id, name } = category;
    return (
      <NavItem className='text-nowrap' key={id}>
        <Link
          href='/products/[category]'
          as={`/products/${formatURL(name)}`}
          passHref
        >
          <NavLink
            onClick={() => handleChangeCategory(name)}
            // Check active link
            className={
              (router.query.category === formatURL(name) ? 'active' : 'border')
            }
          >
            {name}
          </NavLink>
        </Link>
      </NavItem>
    );
  });

  return (
    // Not display until loading done
    !categories.isLoading && (
      <Nav pills className='nav-justified' id='category-bar'>
        <NavItem className='text-nowrap'>
          <NavLink>Filter by:</NavLink>
        </NavItem>
        {categoriesList}
      </Nav>
    )
  );
}

export default Categories;
