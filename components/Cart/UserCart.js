/* eslint-disable no-restricted-globals */
import React, { Component } from 'react';
import { connect } from 'react-redux';
// import "./cart.style.scss";
import CartItem from './CartItem';
import { clearCart } from '../../actions/cart.actions';
import Link from 'next/link';
import { Button, Table } from 'reactstrap';

class Cart extends Component {
  render() {
    const { cart, auth } = this.props;

    const handleClearCart = () => {
      if (confirm('Are you sure ?')) {
        this.props.clearCart(auth.token);
      }
    };

    const cartItems = cart.cart_detail.map(item => {
      return <CartItem {...item} key={item.product.id} />;
    });
    return (
      <Table id='cart' striped hover responsive>
        <thead>
          <tr>
            <th>Product</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Subtotal</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {/* Check if cart is empty */}
          {cartItems.length ? (
            cartItems
          ) : (
            <tr>
              <td colSpan='5' className='text-center text-info h4'>
                Cart is empty !
              </td>
            </tr>
          )}
        </tbody>
        <tfoot>
          <tr>
            <td className='px-0'>
              <Link href='/products/[category]' as='/products/All' shallow>
                <Button color='warning' size='lg'>
                  <i className='fa fa-chevron-circle-left fa-lg mr-1'></i>
                  {/* Continue Shopping */}
                  Back to shop
                </Button>
              </Link>
              <Button
                color='danger'
                className='ml-md-1 mt-1 mt-md-0'
                size='lg'
                onClick={handleClearCart}
              >
                <i className='fa fa-times fa-lg mr-1'></i>Clear cart
              </Button>
            </td>
            <td colSpan='2' className='text-center'>
              <strong>Total ${cart.total.toLocaleString('en-EN')}</strong>
            </td>
            <td colSpan='2' className='px-0'>
              <Link href='/checkout'>
                <Button color='success' size='lg' block>
                  <i className='fa fa-cart-arrow-down fa-lg mr-2'></i>
                  Checkout
                </Button>
              </Link>
            </td>
          </tr>
        </tfoot>
      </Table>
    );
  }
}

const mapStateToProps = state => ({
  cart: state.cartReducer,
  auth: state.authReducer
});

const mapDispatchToProps = dispatch => ({
  clearCart: token => dispatch(clearCart(token))
});

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
