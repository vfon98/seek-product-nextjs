import React from 'react';
import { useRouter } from 'next/router'
import { Card, CardBody, CardFooter, Button, CardTitle, CardText } from 'reactstrap'

const OtherProducts = props => {
  const router = useRouter();
  const { products } = props;

  const renderOtherProducts = () => {
    return products.map(product => (
      <Card className="col-md-2" key={product.id}
        onClick={() => router.push('/products/detail/[id]', `/products/detail/${product.id}`)}
      >
        <img
          className="card-img-top border-bottom img-fluid"
          src={product.image || 'https://place-hold.it/200'}
          alt="Product image"
        />
        <CardBody>
          <CardTitle>{product.name}</CardTitle>
          <CardText className="text-muted font-weight-bold">
            ${product.price.toLocaleString('en-EN')}
          </CardText>
        </CardBody>
        <CardFooter>
          <Button color='success'><i className="fa fa-info-circle mr-2"></i>Details</Button>
        </CardFooter>
      </Card>
    ));
  };

  return (
    <React.Fragment>
      <div className="d-flex" id="prod-others">
        {renderOtherProducts()}
      </div>
    </React.Fragment>
  );
};

export default OtherProducts;
