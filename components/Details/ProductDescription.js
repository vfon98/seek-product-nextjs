import React, { useState } from 'react';
import { Collapse, Col, Card, CardHeader, CardBody } from 'reactstrap';

const ProductDescription = props => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <Col className='p-0'>
      <Card className='mt-3 shadow'>
        <CardHeader className='text-center bg-info p-0 overflow-hidden'>
          <button
            className='btn text-white font-weight-bold btn-block'
            onClick={() => setIsOpen(!isOpen)}
          >
            Click to show full description
          </button>
        </CardHeader>
        <Collapse isOpen={isOpen} className='overflow-hidden'>
          <CardBody>
            <div dangerouslySetInnerHTML={{ __html: props.description }}></div>
          </CardBody>
        </Collapse>
      </Card>
    </Col>
  );
};

export default ProductDescription;
