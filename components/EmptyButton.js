import React, { Component } from 'react';

export default class EmptyButton extends Component {
  render() {
    return (
      <button className='btn btn-warning btn-lg mt-3 py-2 font-weight-bold align-content-center d-flex'>
        <i className='fa fa-frown-o fa-lg mr-2 fa-2x'></i>Empty category !
      </button>
    );
  }
}
