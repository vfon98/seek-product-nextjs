/* eslint-disable no-restricted-globals */
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Link from 'next/link';
import { useRouter } from 'next/router';
import {
  Container,
  Nav,
  Navbar as NavBar,
  NavItem,
  NavLink,
  Badge,
  Button,
} from 'reactstrap';
import { logout } from '../../actions/auth.actions';
import dynamic from 'next/dynamic';
// import MiniCart from '../Cart/MiniCart';
const MiniCart = dynamic(() => import('../Cart/MiniCart'));
import '../../pages/cart/cart.style.scss';

const Navbar = () => {
  const cart = useSelector(state => state.cartReducer);
  const userInfo = useSelector(state => state.userReducer);
  const auth = useSelector(state => state.authReducer);
  const dispatch = useDispatch();
  const router = useRouter();

  const handleLogout = e => {
    e.preventDefault();
    if (confirm('Are you sure to logout ?')) {
      dispatch(logout(auth.token));
      router.push('/login');
    }
  };

  const checkActive = route => {
    return router.pathname.startsWith(route) ? 'active' : '';
  };

  return (
    <NavBar color='dark' dark expand='md' className='fixed-top p-1 pb-md-0'>
      <Container fluid='lg'>
        <Link href='/products/[category]' as='/products/All' passHref shallow>
          <a className='navbar-brand'>Shopping Now</a>
        </Link>

        <Button
          className='navbar-toggler'
          data-toggle='collapse'
          data-target='#collapsibleNavbar'
        >
          <span className='navbar-toggler-icon'></span>
        </Button>

        <Nav
          className='navbar-nav collapse navbar-collapse d-md-flex justify-content-md-end'
          id='collapsibleNavbar'
        >
          <NavItem>
            <Link
              href='/products/[category]'
              as='/products/All'
              passHref
              shallow
            >
              <NavLink className={checkActive('/products')}>
                <i className='fa fa-lg fa-shopping-bag mr-2'></i>Shop
              </NavLink>
            </Link>
          </NavItem>
          <NavItem>
            {/* Link to MiniCart by id */}
            <NavLink id='bs-mini-cart' className={checkActive('/cart')}>
              <i className='fa fa-lg fa-shopping-cart mr-2'></i>My cart
              <Badge color='warning' pill className='ml-1'>
                {cart.cart_detail.length || ''}
              </Badge>
            </NavLink>
            {/* POP-UP CART HERE */}
            <MiniCart />
          </NavItem>
          <NavItem>
            <Link href='/user' passHref>
              <NavLink
                className={checkActive('/user') || checkActive('/update-user')}
              >
                <i className='fa fa-lg fa-user mr-2'></i>My account
              </NavLink>
            </Link>
          </NavItem>
          {/* Logout button */}
          {auth.isLogin && (
            <NavItem title={userInfo.id}>
              <NavLink onClick={handleLogout} href=''>
                <i className='fa fa-lg fa-power-off mr-2'></i>
                {userInfo.firstName}
              </NavLink>
            </NavItem>
          )}
        </Nav>
      </Container>
    </NavBar>
  );
};

export default React.memo(Navbar);
