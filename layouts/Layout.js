import React, { useEffect } from 'react';
import Navbar from '../components/Navbar/Navbar';
import Head from 'next/head';
import { useDispatch, useSelector } from 'react-redux';
import { authUser } from '../actions/user.actions';
import { Container } from 'reactstrap';
import { fetchCart } from '../actions/cart.actions';
import { initFCM } from '../utils/firebase.init';
// import '../index.scss';

const Layout = props => {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.authReducer);

  useEffect(() => {
    dispatch(authUser());
    initFCM();
  }, []);

  useEffect(() => {
    dispatch(fetchCart(auth.token));
  }, [auth.isLogin]);

  return (
    <React.Fragment>
      <Head>
        <title>NextJs - Seekproduct</title>
        <meta
          name='viewport'
          content='width=device-width, initial-scale=1.0'
          key='viewport'
        />
        <meta charSet='UTF-8' key='charset' />
        <meta
          name='Description'
          content='Author: V.Phong, Description: small website for learning'
        ></meta>
        <meta name='theme-color' content='#317EFB' />

        <meta property='og:title' content='Seekproduct website by VPhong' />
        <meta property='og:type' content='website' />
        <meta
          property='og:url'
          content='https://seek-product-nextjs.now.sh/products/All'
        />
        <meta
          property='og:description'
          content='A small website build with NextJs using seekproduct.com api'
        />
        <meta property='og:image' content='/static/images/favicon.png' />
        <meta property='og:app_id' content='297928181164133' />

        <link
          href='https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'
          rel='stylesheet'
          integrity='sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN'
          crossOrigin='anonymous'
        />
        <link rel='manifest' href='/static/manifest.json' />
        <link
          rel='shortcut icon'
          href='/static/images/favicon.png'
          type='image/x-icon'
        />
        <script src='https://js.stripe.com/v3/'></script>
      </Head>
      <Navbar />
      <Container fluid='lg' className='py-4 mt-5'>
        {props.children}
      </Container>
    </React.Fragment>
  );
};

export default Layout;
