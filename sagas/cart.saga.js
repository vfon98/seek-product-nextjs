import { all, put, call, takeEvery, select } from 'redux-saga/effects';
import * as CartAPI from '../api/cart.api';
import * as FirebaseAPI from '../api/firebase.api';
import * as types from '../constants/actionTypes';
import * as actions from '../actions/cart.actions';
import { getUser } from './selectors';
import {
  // getCartFromLocalStorage,
  updateCartInLocalStorage,
  updateGuestCartFromLocalStorage,
  getGuestCartFromLocalStorage,
  removeGuestCartFromLocalStorage,
  getFCMToken,
} from '../utils/local.storage';
import { getCart, getAuth } from './selectors';

export function* fetchCart(action) {
  try {
    const { isLogin } = yield select(getAuth);
    // Fetch guest cart
    if (!isLogin) {
      yield put({
        type: types.FETCH_GUEST_CART_OK,
        payload: { guest_cart: getGuestCartFromLocalStorage() },
      });
      return;
    }

    // Fetch user cart from local
    // put({
    //   type: types.FETCH_CART_OK,
    //   payload: { cart: getCartFromLocalStorage() },
    // });
    // Merge cart into one
    yield call(mergeCart);
    // Fetch user cart from API
    const { token } = action.payload;
    const res = yield call(CartAPI.fetchCart, token);
    yield put({
      type: types.FETCH_CART_OK,
      payload: { cart: res.data },
    });
    yield updateCartInLocalStorage(res.data);
  } catch (err) {
    console.log({ err });
  }
}

function* mergeCart() {
  const auth = yield select(getAuth);
  if (auth.isLogin) {
    const guestCart = yield getGuestCartFromLocalStorage();
    if (guestCart.length) {
      let addingRequests = yield guestCart.map(item =>
        put(actions.addToCart(auth.token, item.product.id, item.amount))
      );
      yield all(addingRequests);
      yield removeGuestCartFromLocalStorage();
    }
  }
}

export function* addToCart(action) {
  try {
    const { token, productID, amount } = action.payload;
    const res = yield call(CartAPI.addToCart, token, productID, amount);

    yield put({
      type: types.ADD_TO_CART_OK,
      payload: { cart: res.data },
    });
  } catch (err) {
    console.log({ err });
  }
}

export function* addToGuestCart(action) {
  try {
    const { product, amount } = action.payload;
    // Standardize cart item structure
    let cart_item = {
      product,
      amount,
      total: product.price * amount,
    };
    const { cart_detail: currentCart } = yield select(getCart);
    const existedItem = currentCart.find(
      item => item.product.id === product.id
    );
    // Update if item existed, of course !
    if (existedItem) {
      yield call(updateGuestCartItem, product.id, existedItem.amount + 1);
    } else {
      yield put({
        type: types.ADD_TO_GUEST_CART_OK,
        payload: { cart_item },
      });

      const { cart_detail } = yield select(getCart);
      updateGuestCartFromLocalStorage(cart_detail);
    }
  } catch (err) {
    console.log({ err });
  }
}

export function* updateCartItem(action) {
  try {
    const { token, productID, amount } = action.payload;
    const { isLogin } = yield select(getAuth);
    if (!isLogin) {
      yield call(updateGuestCartItem, productID, amount);
      return;
    }

    const res = yield call(CartAPI.updateCartItem, token, productID, amount);
    yield put({
      type: types.UPDATE_CART_ITEM_OK,
      payload: { cart: res.data },
    });
  } catch (err) {
    console.log({ err });
  }
}

export function* updateGuestCartItem(productID, amount) {
  yield put({
    type: types.UPDATE_GUEST_CART_OK,
    payload: { productID, amount },
  });

  const { cart_detail } = yield select(getCart);
  updateGuestCartFromLocalStorage(cart_detail);
}

export function* deleteCartItem(action) {
  try {
    const { token, productID } = action.payload;
    const { isLogin } = yield select(getAuth);
    if (!isLogin) {
      yield call(deleteGuestCartItem, productID);
      return;
    }

    const res = yield call(CartAPI.deleteCartItem, token, productID);
    yield put({
      type: types.DELETE_CART_ITEM_OK,
      payload: { cart: res.data },
    });
  } catch (err) {
    console.log({ err });
  }
}

export function* deleteGuestCartItem(productID) {
  yield put({
    type: types.DELETE_GUEST_CART_OK,
    payload: { productID },
  });
}

export function* clearCart(action) {
  try {
    const { isLogin } = yield select(getAuth);
    if (!isLogin) {
      yield call(clearGuestCart);
      return;
    }

    const { token } = action.payload;
    const res = yield call(CartAPI.clearCart, token);
    yield put({
      type: types.CLEAR_CART_OK,
      payload: { cart: res.data },
    });
  } catch (err) {
    console.log({ err });
  }
}

export function* clearGuestCart() {
  yield put({ type: types.CLEAR_GUEST_CART_OK });
  yield removeGuestCartFromLocalStorage();
}

export function* checkoutCart(action) {
  try {
    const { token, stripe_token } = action.payload;
    const { total } = yield select(getCart); // Get cart before checkout
    yield call(CartAPI.checkoutCart, token, stripe_token);
    yield put({ type: types.CHECK_OUT_OK });

    // Push notification from firebase
    const user = yield select(getUser);
    yield call(FirebaseAPI.sendFCM, getFCMToken(), {
      title: 'Checkout successfully',
      body: `
        User ${user.firstName} has paid $${total.toLocaleString('en-EN')} successfully 
        at ${new Date().toLocaleTimeString()} !`,
      icon: '/static/images/user.png',
    });
  } catch (err) {
    console.log({ err });
    yield put({ type: types.CHECK_OUT_FAILED });
  }
}

export default function* root() {
  yield all([
    takeEvery(types.ADD_TO_CART, addToCart),
    takeEvery(types.ADD_TO_GUEST_CART, addToGuestCart),
    takeEvery(types.FETCH_CART, fetchCart),
    takeEvery(types.UPDATE_CART_ITEM, updateCartItem),
    takeEvery(types.DELETE_CART_ITEM, deleteCartItem),
    takeEvery(types.CLEAR_CART, clearCart),
    takeEvery(types.CHECK_OUT_CART, checkoutCart),
  ]);
}
