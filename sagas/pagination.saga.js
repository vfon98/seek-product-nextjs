import { all, call, put, select, takeEvery } from 'redux-saga/effects';
import { getCategories, getPagination } from './selectors';
import * as types from '../constants/actionTypes';
import { searchByCategory } from '../actions/products.actions';

export function* nextPage() {
  try {
    const { currentSelected } = yield select(getCategories);
    yield put({ type: types.NEXT_PAGE_OK });
    yield put(searchByCategory(currentSelected.name));
  } catch (err) {
    console.log({ err });
  }
}

export function* previousPage() {
  try {
    const { currentSelected } = yield select(getCategories);
    yield put({ type: types.PREVIOUS_PAGE_OK });
    yield put(searchByCategory(currentSelected.name));
  } catch (err) {
    console.log({ err });
  }
}

export function* gotoPage(action) {
  try {
    const { page } = action.payload;
    const { currentSelected } = yield select(getCategories);
    yield put({
      type: types.GO_TO_PAGE_OK,
      payload: { page_number: page }
    });
    yield put(searchByCategory(currentSelected.name));
    console.log("OK")
  } catch (err) {
    console.log({ err });
  }
}

export default function* root() {
  yield all([
    takeEvery(types.NEXT_PAGE, nextPage),
    takeEvery(types.PREVIOUS_PAGE, previousPage),
    takeEvery(types.GO_TO_PAGE, gotoPage),
  ]);
}
