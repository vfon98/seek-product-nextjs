export const getCategories = state => state.categoriesReducer;
export const getPagination = state => state.paginationReducer;
export const getCart = state => state.cartReducer;
export const getAuth = state => state.authReducer;
export const getUser = state => state.userReducer;