import { all } from 'redux-saga/effects';

import productSagas from './product.saga';
import categorySagas from './category.saga';
import authSagas from './auth.saga';
import userSagas from './user.saga';
import cartSagas from './cart.saga';
import paginationSagas from './pagination.saga';

export default function* rootSaga() {
  yield all([
    productSagas(),
    categorySagas(),
    authSagas(),
    userSagas(),
    cartSagas(),
    paginationSagas(),
  ]);
}
