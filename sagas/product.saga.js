import {
  call,
  put,
  all,
  select,
  takeLatest,
  takeEvery
} from 'redux-saga/effects';
import * as types from '../constants/actionTypes';
import * as ProductAPI from '../api/product.api';
import { getPagination } from './selectors';

export function* fetchProducts(action) {
  try {
    yield put({ type: types.PRODUCT_PENDING });
    const { category } = action.payload;
    const { currentPage, pageSize } = yield select(getPagination);

    const res = yield call(
      ProductAPI.searchByCategory,
      category,
      currentPage,
      pageSize
    );
    yield put({
      type: types.FETCH_PRODUCTS,
      payload: res.data.results
    });
    // console.log('PRODUCT PUT TO REDUCER', res.data.results);

    yield put({
      type: types.CREATE_PAGINATION,
      payload: { page: res.data }
    });
  } catch (err) {
    console.log({ err });
  }
}

export function* detailProduct(action) {
  try {
    const { id } = action.payload;
    yield put({ type: types.PRODUCT_PENDING });
    const res = yield call(ProductAPI.detailProduct, id);
    yield put({
      type: types.DETAIL_PRODUCT_OK,
      payload: { details: res.data }
    });
  } catch (err) {
    console.log({ err });
  }
}

export default function* root() {
  yield all([
    takeLatest(types.SEARCH_PRODUCTS, fetchProducts),
    // fork(fetchProducts),
    takeEvery(types.DETAIL_PRODUCT, detailProduct)
  ]);
}
