import { call, all, put, takeEvery, select } from 'redux-saga/effects';
import * as CategoryAPI from '../api/category.api';
import * as types from '../constants/actionTypes';
import { getCategories } from './selectors';
import { resetPage } from '../actions/pagination.actions';

export function* fetchCategories() {
  try {
    const res = yield call(CategoryAPI.fetchCategories);
    yield put({
      type: types.FETCH_CATEGORIES_OK,
      payload: res.data,
    });
  } catch (err) {
    console.log({ err });
  }
}

export function* changeSelectedCategory(action) {
  try {
    const { categoryName } = action.payload;
    const { categories } = yield select(getCategories);
    const currentSelected = categories.find(category => {
      return category.name === categoryName;
    });

    yield put({
      type: types.CHANGE_SELECTED_CATEGORY_OK,
      payload: { category: currentSelected },
    });
    // Reset page to 1 to prevent 404
    yield put(resetPage())
  } catch (err) {
    console.log({ err });
  }
}

export default function* root() {
  yield all([
    takeEvery(types.FETCH_CATEGORIES, fetchCategories),
    takeEvery(types.CHANGE_SELECTED_CATEGORY, changeSelectedCategory),
  ]);
}
