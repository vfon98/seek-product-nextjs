import { all, call, put, takeEvery } from 'redux-saga/effects';
import * as UserAPI from '../api/user.api';
import * as types from '../constants/actionTypes';
import { getUserProfile as getUserProfileAction } from '../actions/user.actions';
import Router from 'next/router';
import {
  getTokenFromLocalStorage,
  removeTokenFromLocalStorage
} from '../utils/local.storage';

export function* getUserProfile(action) {
  try {
    const { token } = action.payload;
    const res = yield call(UserAPI.getUserProfile, token);
    yield put({
      type: types.GET_USER_PROFILE_OK,
      payload: { profile: res.data }
    });
  } catch (err) {
    console.log({ err });
  }
}

export function* authUser() {
  try {
    const token = getTokenFromLocalStorage();
    if (!token) {
      return put({ type: types.TOKEN_NOT_FOUND });
    }

    const res = yield call(UserAPI.getUserProfile, token);
    yield put({
      type: types.AUTH_USER_OK,
      payload: { profile: res.data, token }
    });
  } catch (err) {
    console.log('AUTH ERROR');
    yield put({ type: types.TOKEN_EXPIRED });
    yield removeTokenFromLocalStorage();
  }
}

export function* updateProfile(action) {
  try {
    const { token, newProfile } = action.payload;
    yield call(UserAPI.updateProfile, token, newProfile);
    yield put({
      type: types.UPDATE_PROFILE_OK
    });

    yield put(getUserProfileAction(token));
    yield Router.push('/user');
  } catch (err) {
    console.log({ err });
  }
}

export default function* root() {
  yield all([
    takeEvery(types.GET_USER_PROFILE, getUserProfile),
    takeEvery(types.AUTH_USER, authUser),
    takeEvery(types.UPDATE_PROFILE, updateProfile)
  ]);
}
