import { all, put, call, takeEvery } from 'redux-saga/effects';
import * as AuthAPI from '../api/auth.api';
import * as types from '../constants/actionTypes';
import { getUserProfile } from '../actions/user.actions';
import { saveTokenIntoLocalStorage, removeTokenFromLocalStorage } from '../utils/local.storage';

export function* registerAccount(action) {
  try {
    const { accountInfo } = action.payload;
    const res = yield call(AuthAPI.registerAccount, accountInfo);
    yield put({
      type: types.REGISTER_OK,
      payload: { success: res.data }
    });
  } catch (err) {
    console.log({ err });
    // Existed email or password not match
    yield put({
      type: types.REGISTER_FAILED,
      payload: { error: err.response.data }
    });
  }
}

export function* loginUser(action) {
  try {
    const { email, password } = action.payload;
    const res = yield call(AuthAPI.login, email, password);
    yield put({
      type: types.LOGIN_OK,
      payload: { token: res.data.token }
    });
    yield put(getUserProfile(res.data.token));
    yield saveTokenIntoLocalStorage(res.data.token);
  } catch (err) {
    console.log({ err });
    yield put({
      type: types.LOGIN_FAILED,
      payload: { error: err.response.data }
    });
  }
}

export function* logout(action) {
  console.log("LOGOUT", action)
  try {
    const { token } = action.payload;
    yield removeTokenFromLocalStorage();
    yield call(AuthAPI.logout, token);
    yield put({
      type: types.LOGOUT_OK
    });
  } catch (err) {
    console.log({ err });
  }
}

export function* updatePassword(action) {
  try {
    console.log("IN SAGA")
    const { token, body } = action.payload;
    yield call(AuthAPI.updatePassword, token, body)
    yield put({ type: types.UPDATE_PASSWORD_OK })
  } catch (err) {
    console.log({ err })
    yield put({
      type: types.UPDATE_PASSWORD_FAILED,
      payload: { error: err.response.data }
    })
  }
}

export default function* root() {
  yield all([
    takeEvery(types.LOGIN_ACCOUNT, loginUser),
    takeEvery(types.LOGOUT, logout),
    takeEvery(types.REGISTER_ACCOUNT, registerAccount),
    takeEvery(types.UPDATE_PASSWORD, updatePassword),
  ]);
}
